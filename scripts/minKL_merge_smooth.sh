#!/bin/bash

#### ---------------
# edit with your own path
rootDir=/mypath/
#### ---------------

# Loop trough hemispheres

hemis='l r'

resultsDir=$rootDir/results/blueprint/

for h in $hemis; do #loop trough hemis

	# merge all blueprint minKL
	$WBDIR/wb_command -metric-merge $resultsDir/human_macaque_all_minKLTemp.${h}.func.gii \
	-metric $resultsDir/human_macaque_non_longitudinal_minKLTemp.${h}.func.gii \
	-metric $resultsDir/human_macaque_add_mdlf_ifof_ilf_minKLTemp.${h}.func.gii \
	-metric $resultsDir/human_macaque_add_af_minKLTemp.${h}.func.gii \
	-metric $resultsDir/human_macaque_add_af_ilflat_minKLTemp.${h}.func.gii \
	-metric $resultsDir/human_macaque_add_af_ilfmed_minKLTemp.${h}.func.gii

	# smooth
	$WBDIR/wb_command -metric-smoothing $rootDir/data/human_macaque.${h}.inflated.surf.gii \
	$resultsDir/human_macaque_all_minKLTemp.${h}.func.gii 2 \
	$resultsDir/human_macaque_all_minKLTemp_smooth.${h}.func.gii
	
done #hemis