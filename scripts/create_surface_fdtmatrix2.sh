#!/bin/bash

#### ---------------
# edit with your own path
rootDir=/mypath/
#### ---------------

# Loop trough species and hemispheres and subjects

species='macaque human'
# possibilities: macaque human

hemis='l r'

for s in $species; do # loop trough species
	# define for each species the standard space, the subjects name and surface
	if [ "$s" = "macaque" ]; then
		hemis='l r'
		std=$rootDir/data/$s/F99_10mm
		subjects='01 02 03 04'
		surf=$rootDir/data/$s/${hemi}.midthickness.surf.gii #use the same one for all subjects in macaque
	elif [ "$s" = "human" ]; then
		hemis='l r'
		std=$rootDir/data/$s/MNI152_T1_2mm
		subjects='01 02 03 04 05 06 07 08 09 10'
	fi

	for h in $hemis; do
		for subj in $subjects;do # loop trough subjects
			resultsDir=$rootDir/results/blueprint/$s/subjects/subject_${subj}/surf_seed_${h}
			mkdir -p $resultsDir

			bedpostDir=$rootDir/data/$s/subject_${subj}/dMRI.bedpostX #bedpost directory
			std2dMRI=$rootDir/data/$s/subject_${subj}/standard_to_dMRI_warp #warp from standard space to diffusion space
			dMRI2std=$rootDir/data/$s/subject_${subj}/dMRI_to_standard_warp #warp from diffusion space to standard space
			if [ "$s" = "human" ]; then
				surf=$rootDir/data/$s/subject_${subj}/subject_${subj}.${hemi}.midthickness.32k_fs_LR.surf.gii #use different one for humans
			fi

			quicktrack_gpu $bedpostDir $resultsDir \
				--xfm=$std2dMRI \
				--invxfm=$dMRI2std \
				--seed=$surf \
				--omatrix2 \
				--target2=${std}_brain \
				-P 10000 \
				--seedref=$std
     
		done # subjects
	done #hemis
done #species
