%% Run kmeans clustering for all species, hemispheres and sections

%% ----------------
% edit with your own path
rootDir = '/mypath/';
%% ----------------


%% Loop trough species, hemispheres, ROI sections
species = {'macaque', 'human', 'chimpIV', 'chimpPM', 'gorilla'};
% possibilities: 'macaque', 'human', 'chimpIV', 'chimpPM'
sections = {'ant','mid','post'};
% possibilities: 'ant','mid','post'

for s = 1:length(species) % Loop trough species
    specie = species{s};
    resultsDir = [rootDir '/results/clustering/' specie];
    switch specie % define the hemispheres and subjects for each species
        case 'macaque'
            hemis = {'L', 'R'};
            subjects = {'01','02','03','04'};
        case 'human'
            hemis = {'L', 'R'};
            subjects = {'01','02','03','04','05','06','07','08','09','10'};
        case 'chimpIV'
            hemis = {'L', 'R'};
            subjects = {'01','02','03'};
        case 'chimpPM'
            hemis = {'R'}; % only right available
            subjects = {'01'};
        case 'gorilla'
            hemis = {'R'}; % only right available
            subjects = {'01'};
    end
    
    for subj = 1:length(subjects) % loop trough subjects
        subject = subjects{subj};
        for h = 1:length(hemis) % loop trough hemispheres
            hemi = hemis{h};
            for sec = 1:length(sections) % loop trough sections
                section = sections{sec};
                
                matrix_dir = [resultsDir '/subject_' subject '/outputMat_' hemi '_' section]; %output directory of probtrackx
                fname_fdt_matrix2 = fullfile(matrix_dir, 'fdt_matrix2.dot');
                fname_fdt_paths = fullfile(matrix_dir, 'fdt_paths.nii.gz');
                fname_coords_for_fdt_matrix2 = fullfile(matrix_dir, 'coords_for_fdt_matrix2');
                
                switch section %define the number of clusters based on section (anterior one more because of uncinate)
                    case 'ant'
                        nClusters = 5;
                    otherwise
                        nClusters = 4;
                end
                switch specie % macaque one less because no sub-division of ILF
                    case 'macaque'
                    nClusters = nClusters-1;
                end
                % define the output directory
                outDir = [resultsDir '/subject_' subject '/outputKmeans_' hemi '_' section '/Clusters' nClusters];
                % run kmeans_fdt from MrCat
                kmeans_fdt(fname_fdt_matrix2,fname_fdt_paths,fname_coords_for_fdt_matrix2,nClusters, 'outdir', outDir, 'create_probtrack_masks', 'yes');  
            end % sections
        end % hemis
    end % subjects
end % species
