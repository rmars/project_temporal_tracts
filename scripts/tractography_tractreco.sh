#!/bin/bash

#### ---------------
# edit with your own path
rootDir=/mypath/
#### ---------------

# Loop trough species and hemispheres and tracts

species='macaque human chimpIV chimpPM gorilla'
# possibilities: macaque human chimpIV chimpPM gorilla

for s in $species; do # loop trough species
	# define for each species the hemispheres to use, the standard space and the subjects name
	# define the step length to use by tractography and the resolution to downsample to
	# define what kind of stop mask (surfacefile or nifti file)
	if [ "$s" = "macaque" ]; then
		hemis='l r'
		std=$rootDir/data/$s/F99_05mm_brain
		subjects='01 02 03 04'
		tracts='af ifo ilf mdlf'
		step_length=0.25
		res=1
		GM_ext=.nii.gz
	elif [ "$s" = "human" ]; then
		hemis='l r'
		std=$rootDir/data/$s/MNI152_T1_1mm_brain
		subjects='01 02 03 04 05 06 07 08 09 10'
		tracts='af ifo ilflat ilfmed mdlf'
		step_length=0.5
		res=2
		GM_ext=.surf.gii
	elif [ "$s" = "chimpIV" ]; then
		hemis='l r'
		std=$rootDir/data/$s/ChimpYerkes29_T1w_08mm_brain
		subjects='01 02 03'
		tracts='af ifo ilflat ilfmed mdlf'
		step_length=0.5
		res=1
		GM_ext=.surf.gii
	elif [ "$s" = "chimpPM" ]; then 
		hemis='r' # only the right hemispehre is available
		# only one individual so stay in individual space
		subjects='01'
		tracts='af ifo ilflat ilfmed mdlf'
		step_length=0.3
		GM_ext=.nii.gz
	elif [ "$s" = "Gorilla" ]; then
		hemis='r' # only the right hemispehre is available
		# only one individual so stay in individual space
		subjects='01'
		tracts='af ifo ilflat ilfmed mdlf'
		step_length=0.3
		GM_ext=.nii.gz
	fi

	protocolDir=$rootDir/masks/protocols_tracts/$s
	averageDir=$rootDir/results/tractreco/$s/average
	for subj in $subjects;do # loop trough subjects
		bedpostDir=$rootDir/data/$s/subject_${subj}/dMRI.bedpostX #bedpost directory
		resultsDir=$rootDir/results/tractreco/$s/subjects/subject_${subj} #results directory
		binary_mask=$bedpostDir/nodif_brain_mask #binary mask defined as the no diffusion mask from bedpost output
		
		for h in $hemis;do # loop trough hemispheres
			gm_stop_mask=$rootDir/data/$s/subject_${subj}/GM_mask_${h}${GM_ext} # grey matter mask in subject space
			for tract in $tracts;do # loop trough tracts
				echo "TRACTOGRAPHY $s subject_${subj} ${tract}_${h}"
				# combining the target masks
				t=${tract}_${h}
				fslmaths $protocolDir/$t/targetA -add $protocolDir/$t/targetP $protocolDir/$t/target

				if [ "$s" = "chimpPM" ] || [ "$s" = "gorilla" ]; then
					# run probtrackx2 from FSL with option
					probtrackx2 --samples=$bedpostDir/cudimot_Param/merged \
						-m $binary_mask \
						-x $protocolDir/$t/seed.nii.gz \
						--opd \
						--ompl \
						-o density \
						--loopcheck \
						--forcedir \
						--dir=$resultsDir/$t \
						--avoid=$protocolDir/$t/exclude.nii.gz \
						--waypoints=$protocolDir/$t/target.nii.gz \
						--stop=$gm_stop_mask \
						--steplength=$step_length \
						--waycond='OR' \
						--nsamples=10000 \
						--sampvox=1


				else
					std2dMRI=$rootDir/data/$s/subject_${subj}/standard_to_dMRI_warp #warp from standard space to diffusion space
					dMRI2std=$rootDir/data/$s/subject_${subj}/dMRI_to_standard_warp #warp from diffusion space to standard space

					# run probtrackx2 from wrapper autoptx_prob
					probtrackx2 --samples=$bedpostDir/merged \
						-m $binary_mask \
						-x $protocolDir/$t/seed.nii.gz \
						--xfm=$std2dMRI --invxfm=$dMRI2std \
						--seedref=$std \
						--opd \
						--ompl \
						-o density \
						--loopcheck \
						--forcedir \
						--dir=$resultsDir/$t \
						--avoid=$protocolDir/$t/exclude.nii.gz \
						--waypoints=$protocolDir/$t/target.nii.gz \
						--stop=$gm_stop_mask \
						--steplength=$step_length \
						--waycond='OR' \
						--nsamples=10000 \
						--sampvox=1

				fi

				echo "NORMALISATION $s subject_${subj} ${tract}_${h}"

				fslmaths $resultsDir/$t/density -div `cat $resultsDir/$t/waytotal` $resultsDir/$t/densityNorm
# 
		
			done # loop tracts done
		done # loop hemispheres done

	done # loop subjects done

	for h in $hemis;do # loop trough hemispheres
		for tract in $tracts;do # loop trough tracts
		echo "AVERAGE $s ${tract}_${h}"
		t=${tract}_${h}
		mkdir -p $averageDir/$t

		if [ "$s" = "chimpPM" ] || [ "$s" = "gorilla" ]; then
			# no need to average or downsample
			cp $rootDir/results/tractreco/$s/subjects/*/$t/densityNorm.nii.gz $averageDir/$t/densityNorm_lr.nii.gz

		else
			#average across subjects
			fsladd $averageDir/$t/densityNorm -m $rootDir/results/tractreco/$s/subjects/*/$t/densityNorm.nii.gz
			# lower res
			flirt -in $averageDir/$t/densityNorm -ref $std -out $averageDir/$t/densityNorm_lr -applyisoxfm $res
		fi


		# log transform
		fslmaths $averageDir/$t/densityNorm_lr -mul 100000000000 -add 1 $averageDir/$t/densityNorm_lr_mul
		fslmaths $averageDir/$t/densityNorm_lr_mul -log $averageDir/$t/densityNorm_lr_log
		fslmaths $averageDir/$t/densityNorm_lr_log  -div `fslstats $averageDir/$t/densityNorm_lr_log -R | awk '{print $2}'` $averageDir/$t/densityNorm_lr_lognorm
		
		rm $averageDir/$t/densityNorm_lr_mul.nii.gz
		rm $averageDir/$t/densityNorm_lr_log.nii.gz

		done # loop tracts done

		# add the two ILf tracts for blueprint
		if [ "$s" = "human" ]; then
			mkdir -p $averageDir/ilf2_${h}
			fslmaths $averageDir/ilflat_${h}/densityNorm_lr -add $averageDir/ilfmed_${h}/densityNorm_lr $averageDir/ilf2_${h}/densityNorm_lr
		fi
	done # loop hemispheres done

done # loop species done
