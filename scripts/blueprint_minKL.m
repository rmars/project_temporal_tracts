%% Average fdt_matrix2

%% ----------------
% edit with your own path
rootDir = '/mypath/';
%% ----------------

%% Loop trough species, hemispheres, type of blueprint
species = {'macaque', 'human'};
hemis = {'l','r'};
Hemis = {'L','R'}; % for the readimgfile and saveimgfile
resultsDir = [rootDir 'results/blueprint/'];

%% Mask with temporal lobe mask
for s = 1:length(species) % Loop trough species
    specie = species{s};
    for h = 1:length(hemis) % Loop trough hemis
        hemi = hemis{h};
        Hemi = Hemis{h};
        temporal_mask = readimgfile([rootDir 'data/' specie '/Tempmask.' hemi '.func.gii'],Hemi);
        
        switch specie
            case 'macaque'
                blueprints = {'non_longitudinal','add_ifof_ilf_mdlf','add_af'};
            case 'human'
                blueprints = {'non_longitudinal','add_ifof_ilf_mdlf','add_af','add_af_ilflat', 'add_af_ilfmed'}; %human blueprints to compare to macaque
        end
        for b = 1:length(blueprints) % Loop trough blueprints
            blueprint = blueprints{b};
            blueprint_whole = readimgfile([resultsDir specie '/' blueprint '.'  hemi '.dtseries.nii'],Hemi);
            findbintemp = find(temporal_mask == 1);
            blueprint_masked = blueprint_whole(findbintemp); %restrict to temporal 
            blueprint_masked_dim = zeros(size(blueprint_masked)); %correct dimension for the blueprint
            for i = 1:size(blueprint_whole,2) % going trough the columns (tracts)
                blueprint_masked_dim(findbintemp,i) = blueprint_whole(findbintemp,i);
            end
            saveimgfile(blueprint_masked_dim,[resultsDir specie '/' blueprint '_tempmask.'  hemi '.dtseries.nii'],Hemi);
        end %blueprints
    end %hemis
end %species


%% Calculate min KL maps
blueprints_m = {'non_longitudinal','add_ifof_ilf_mdlf','add_af','add_af','add_af'};
blueprints_h = {'non_longitudinal','add_ifof_ilf_mdlf','add_af','add_af_ilflat', 'add_af_ilfmed'}; 

for h = 1:length(hemis) % Loop trough hemis
    hemi = hemis{h};
    for b = 1:length(blueprints_m) % Loop trough blueprints
        blueprint_m = blueprints_m{b};
        blueprint_h = blueprints_h{b};
        mac_blueprints = readimgfile([resultsDir 'macaque/' blueprint_m '_tempmask.'  hemi '.dtseries.nii'],Hemi);
        hum_blueprints = readimgfile([resultsDir 'human/' blueprint_h '_tempmask.'  hemi '.dtseries.nii'],Hemi);
        HM = calc_KL(hum_blueprints,mac_blueprints);
        MH = calc_KL(mac_blueprints,hum_blueprints);
        HMtmp=HM; HMtmp(HM==0)=1e10; % Using trick to deal with zeros
        MHtmp=MH; MHtmp(MH==0)=1e10;
        grot1=min(HMtmp,[],2);  grot1(grot1==1e10)=0;
        grot2=min(MHtmp,[],2);  grot2(grot2==1e10)=0;
        
        saveimgfile([grot1;grot2],[resultsDir 'human_macaque' blueprint_h '_minKLTemp.' hemi '.func.gii'], Hemi);

    end %blueprints
    
end %hemis

