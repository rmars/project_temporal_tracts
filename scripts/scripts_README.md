# Longitudinal connections and the organization of the temporal cortex in macaques, great apes, and humans

## Roumazeilles L, Eichert N, Bryant KL, Folloni D, Sallet J, Vijayakumar S, Foxley S, Tendler BC, Jbabdi S, Reveley C, Verhagen L, Dershowitz LB, Guthrie M, Flach E, Miller KL, Mars RB.


Explanations to use the scripts used for the results in the paper.
Some scripts call other scripts from MrCat toolbox available at: https://github.com/neuroecology/MrCat


# Connectivity-based clustering

**1. Tractography for clustering**

relevant scripts: ```tractography_for_clustering.sh```

relevant files: in masks/clustering_ROIs

data needed: bedpostX folder with nodif_brain_mask and a downsampled version, standard image, warps from standard space to diffusion space and inversly

This step performs tractography from the ROI to the whole brain and output a matrix representing the connectivity of each voxel in the ROI to the rest of the brain.

**2. k-means clustering**

relevant scripts: ```wrapper_kmeans_clustering.m```
(calls kmeans_fdt.m from MrCat/core)

relevant files: output of the previous step

This step performs k-means clustering, it clusters together voxels in the ROI which have similar connectivity profile to the rest of the brain.

**3. Label each of the clusters obtained and add them over subjects for each tracts**

Clusters can be added using fslmaths from FSL.

The results are available in results/clustering.

# Tract reconstruction

**1. Make the masks from the clustering results**

The tractography masks (seed and waypoints) can be made from the clustering results.
They are available in masks/protocols_tracts, together with the exclusion masks.

**2. Tractography**

relevant scripts: ```tractography_tractreco.sh```

relevant files: in masks/protocols_tract

data needed: bedpostX folder with nodif_brain_mask, standard images, warps from standard space to diffusion space and inversly and a grey_matter mask for each subject

This step performs tractography from the seed to waypoints for each tracts of each subject, normalise the tract, average across subjects and log transformed the results.

The results are available in results/tractreco/$species/average

# Blueprint for macaques and humans

**1. Create the connectivity matrix**

relevant scripts: ```create_surface_fdtmatrix2.sh```
(calls quicktract_gpu)

data needed: bedpostX folder with nodif_brain_mask, standard images, warps from standard space to diffusion space and inversly and a surface midthickness for each subject in humans and a standard one for macaques.

This step creates a matrix defining the connectivity of each vertex of the GM to the rest of the brain.

**2. Average fdt_matrix2**

relevant scripts: ```average_fdt_matrix2.m```

This step average the matrices obtained at the previous stages across subjects.

**3. Create the blueprints**

relevant scripts: ```create_blueprint.m```

relevant files: in results/tractreco/$species/average and averaged matrices from previous step.

This step creates the necessary blueprints to be compared.

**4. Calculate the KL minimum maps**

relevant scripts: ```blueprint_minKL.m``` 
(using readimgfile.m, saveimgfile.m, calc_KL.m)

relevant files: created at previous step in results/blueprint/$species/ and a mask file in data/$species/Tempmask.$hemi.func.gii if you want to restrict to the temporal lobe.

This step calculates the minimum KL between blueprints of macaque and humans.

**5. Merge blueprints for figure**

relevant scripts: ```minKL_merge_smooth.sh```

relevant files: created at previous step in results/blueprint

data needed: the human macaque combined surface

This step merges the minimum KL maps and smooth them.
The results can be found in results/blueprint/human_macaque_all_minKLTemp_smooth.$hemi.func.gii

**6. Visualisation**

Open in workbench viewer results/blueprint/minKL_maps.scene.
This scene reproduces the KL maps found in figure 6, 7, S6 and S7.
You will need to clone the whole project to read the scene file.

